package com.challenge.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.challenge.utils.Utils.pushData;

public class PriceSmartProductPage extends BasePage{

    @FindBy(xpath = "//*[@id='clubs-selection']//span[text() = 'Bogotá Salitre']")
    private WebElement salitreClub;

    @FindBy(xpath = "//*[@id='clubs-selection']//span[text() = 'Bogotá Salitre']/preceding-sibling::i")
    private WebElement stockMark;

    @FindBy(id = "product-display-name")
    private WebElement productTitle;

    @FindBy(id = "product-price")
    private WebElement productPrice;

    public PriceSmartProductPage(WebDriver webDriver, String productId) {
        super(webDriver);
        webDriver.get("https://www.pricesmart.com/site/co/es/pagina-producto/" + productId);
    }

    public String getProductTitle() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(productTitle));
        return productTitle.getText();
    }
    public boolean isProductAvailable() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(salitreClub));
        getWebDriverWait().until(ExpectedConditions.visibilityOf(stockMark));
        return stockMark.getAttribute("class").contains("fa-check");
    }

    public String getProductPrice() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(productPrice));
        return productPrice.getText();
    }
}
