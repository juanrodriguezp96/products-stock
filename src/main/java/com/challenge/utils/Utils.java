package com.challenge.utils;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Utils {

    private static List<String[]> data;

    public static List<String[]> getData() {
        return data;
    }

    public static void setData(List<String[]> data) {
        Utils.data = data;
    }

    public static void pushData(String[] data) {
        Utils.data.add(data);
    }

    public static void writeDataLineByLine(String filePath, List<String[]> data)
    {
        // first create file object for file placed at location
        // specified by filepath
        File file = new File(filePath);
        try {
            // create FileWriter object with file as parameter
            FileWriter outputfile = new FileWriter(file);

            // create CSVWriter object filewriter object as parameter
            CSVWriter writer = new CSVWriter(outputfile);

            // add data to csv
            data.forEach(writer::writeNext);

            // closing writer connection
            writer.close();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
