@Run
Feature: PriceSmart
  Validation of stock

  Scenario Outline: Verify product stock
    Given I am in the PriceSmart product "<productId>" page
    When I check the product "<productId>" is available
    #Then I check the price of "<productId>" is "<price>"
    Examples:
      | productId | price   |
      | 335514    | 38.900  |
      | 755710    | 154.900 |
      | 7136      | 35.900  |
      | 5634      | 20.500  |
      | 524550    | 26.900  |
      | 777860    | 66.900  |
      | 9180      | 32.500  |
      | 172794    | 66.900  |
      | 1622      | 17.700  |
      | 388039    | 65.900  |
      | 781060    | 69.900  |
      | 38        | 23.700  |
      | 91607     | 41.900  |
      | 801190    | 32.900  |
      | 343211    | 21.900  |
      | 16724     | 43.900  |
      | 36633     | 27.500  |
      | 151223    | 43900   |
      | 151224    | 43900   |
      | 322046    | 17900   |
      | 14381     | 94.500  |
      | 16555     | 83.500  |
      | 165530    | 83.500  |
      | 931315    | 25.900  |
      | 326236    | 44.900  |
      | 877900    | 81.900  |
      | 16250     | 29.900  |
      | 320015    | 19.500  |
      | 411228    | 36.900  |
      | 392311    | 79.900  |
      | 412983    | 61.500  |
      | 314402    | 101.900 |
      | 306781    | 66.900  |
      | 25444     | 44.900  |
      | 187890    | 67.500  |
      | 408975    | 38.500  |
      | 189924    | 16.500  |
      | 325519    | 27.900  |
      | 362531    | 33.900  |
      | 303299    | 35.900  |
      | 361343    | 40.500  |
      | 9819      | 244.900 |
      | 9415      | 64.500  |
      | 19369     | 39.900  |
      | 400425    | 33.900  |
      | 335474    | 51.500  |
      | 33        | 53.500  |
      | 6217      | 27.900  |
      | 322398    | 29.500  |
      | 8409      | 97.900  |
      | 435306    | 45.900  |
      | 172768    | 44.900  |
      | 746747    | 58.900  |
      | 17948     | 19.900  |
      | 376052    | 79.900  |
      | 350013    | 20.900  |
      | 439676    | 24.500  |
      | 298531    | 57.900  |
      | 299532    | 67.500  |
      | 389603    | 67.900  |
      | 165547    | 79.900  |
      | 13        | 42.900  |
      | 420638    | 27.900  |
      | 416100    | 27.900  |
      | 141042    | 57.900  |
      | 2931      | 25.700  |
      | 35486     | 68.900  |
      | 14288     | 53.500  |
      | 11188     | 39.900  |
      | 333292    | 13.500  |
      | 128       | 46.900  |
      | 15615     | 45.900  |
      | 2044      | 99.900  |
      | 223733    | 57.900  |
      | 430366    | 14.700  |
      | 420753    | 35.900  |
      | 6926      | 54.900  |
      | 424496    | 146.900 |
      | 415277    | 149.900 |
      | 347251    | 35.900  |
      | 335238    | 108.500 |
      | 314425    | 157.900 |
      | 166155    | 64.900  |
      | 408973    | 38.500  |
      | 288882    | 66.900  |
      | 55332     | 34.900  |
      | 5532      | 32.500  |
      | 396173    | 39.900  |
      | 211808    | 26.900  |
      | 425773    | 37.500  |
      | 538003    | 58.900  |
      | 419092    | 35.900  |
      | 333712    | 51.500  |
      | 19788     | 18.900  |
      | 85196     | 44.900  |
      | 319131    | 40.500  |
      | 369029    | 25.900  |
      | 344607    | 39.900  |
      | 223732    | 55.900  |
      | 365096    | 17.500  |
      | 24685     | 39.900  |
      | 380592    | 39.900  |
      | 335423    | 78.900  |
      | 347250    | 29.500  |
      | 417174    | 41.500  |
      | 28615     | 28.900  |
      | 16551     | 75.900  |
      | 240158    | 99.900  |
      | 601509    | 105.900 |
      | 16114     | 61.500  |
      | 361339    | 44.900  |
      | 395075    | 104.900 |
      | 426487    | 18.700  |
      | 19785     | 20.500  |
      | 339625    | 17.900  |
      | 9265      | 53.900  |
      | 438623    | 45.900  |
      | 998809    | 49.900  |
      | 55331     | 32.500  |
      | 434859    | 248.900 |
      | 359319    | 51.500  |
      | 25231     | 61.900  |
      | 858579    | 25.500  |
      | 3977      | 41.900  |
      | 347638    | 13.500  |
      | 27398     | 42.900  |
      | 88333     | 35.500  |
      | 392124    | 76.500  |
      | 205486    | 54.500  |
      | 366116    | 28.500  |
      | 329519    | 20.500  |
      | 3468      | 45.900  |
      | 448005    | 118.500 |
      | 436261    | 43.900  |
      | 931549    | 72.900  |
      | 434352    | 13.900  |
      | 16139     | 58.500  |
      | 11794     | 52.900  |
      | 467575    | 64.900  |
      | 501111    | 57.900  |
      | 322399    | 22.900  |
      | 323964    | 30.500  |
      | 321400    | 92.900  |
      | 538000    | 86.900  |
      | 345535    | 25.900  |
      | 349541    | 22.500  |
      | 322402    | 35.900  |
      | 424829    | 100.500 |
      | 333905    | 71.900  |
      | 777766    | 40.500  |
      | 198916    | 31.900  |
      | 314424    | 154.900 |
      | 172784    | 74.900  |
      | 415268    | 145.900 |
      | 108117    | 48.900  |
      | 428679    | 169.900 |
      | 436258    | 52.900  |
      | 436259    | 60.500  |
      | 33996     | 189.900 |
      | 8754      | 32.500  |
      | 999003    | 44.900  |
      | 410643    | 31.900  |
      | 314423    | 154.900 |
      | 71268     | 44.900  |
      | 316290    | 89.900  |
      | 417171    | 41.500  |
      | 732581    | 93.900  |
      | 385561    | 39.900  |
      | 732577    | 79.900  |
      | 29284     | 56.900  |
      | 427338    | 137.900 |
      | 404225    | 86.900  |
      | 419755    | 47.900  |
      | 979855    | 93.900  |
      | 38866     | 35.900  |
      | 354400    | 46.900  |
      | 19139     | 77.900  |
      | 19127     | 79.900  |
      | 603199    | 33.900  |
      | 436927    | 24.700  |
      | 343115    | 37.900  |
      | 370505    | 43.900  |
      | 20263     | 58.500  |
      | 410968    | 58.900  |
      | 108095    | 22.900  |
      | 168191    | 66.500  |
      | 5800      | 46.500  |
      | 5521      | 11.900  |
      | 210257    | 57.900  |
      | 6196      | 38.900  |
      | 12677     | 45.500  |
      | 440429    | 25.500  |
      | 102001    | 31.900  |
      | 388043    | 55.900  |
      | 1543      | 87.900  |
      | 427256    | 35.900  |
      | 419189    | 80.700  |
      | 1128      | 84.900  |
      | 3084      | 41.900  |
      | 415632    | 29.700  |
      | 70634     | 54.900  |
      | 402138    | 31.900  |
      | 388906    | 43.900  |
      | 815248    | 45.900  |
      | 22388     | 75.900  |
      | 434341    | 77.500  |
      | 26219     | 31.900  |
      | 195622    | 134.900 |
      | 702621    | 67.900  |
      | 23393     | 46.900  |
      | 7165      | 35.900  |
      | 388073    | 69.900  |
      | 177       | 43.900  |
      | 161       | 24.900  |
      | 5046      | 66.900  |
      | 262427    | 29.900  |
      | 11207     | 27.900  |
      | 705170    | 42.900  |
      | 343096    | 56.900  |
      | 223722    | 110.500 |
      | 140778    | 63.900  |
      | 136482    | 31.900  |
      | 501333    | 53.900  |
      | 415000    | 76.900  |
      | 448004    | 114.900 |
      | 404087    | 64.900  |
      | 404097    | 64.900  |
      | 413260    | 62.500  |
      | 240159    | 67.900  |
      | 333075    | 65.900  |
      | 430366    | 14.700  |
      | 448002    | 94.900  |
      | 219226    | 44.500  |
      | 431697    | 39.900  |
      | 390343    | 66.500  |
      | 351718    | 46.500  |
      | 102002    | 42.900  |
      | 935109    | 44.900  |
      | 25018     | 22.900  |
      | 357817    | 37.900  |
      | 25017     | 22.900  |
      | 755720    | 82.900  |
      | 228941    | 18.900  |
      | 754112    | 34.900  |
      | 669653    | 69.900  |
      | 381999    | 30.900  |
      | 5515      | 20.900  |
      | 415498    | 44.700  |
      | 371489    | 28.500  |
      | 315       | 43.500  |
      | 314696    | 152.900 |
      | 436155    | 25.900  |
      | 24325     | 59.900  |
      | 68821     | 174.900 |
      | 4642      | 34.500  |
      | 55501     | 34.900  |
      | 772097    | 33.500  |
      | 55334     | 21.900  |
      | 448003    | 118.500 |
      | 395770    | 139.900 |
      | 16100     | 18.900  |
      | 420379    | 44.900  |
      | 398079    | 59.900  |
      | 755730    | 72.900  |
      | 777768    | 76.500  |
      | 956696    | 124.900 |
      | 3067      | 87.900  |
      | 62413     | 129.900 |
      | 434365    | 28.900  |
      | 831528    | 104.500 |
      | 318336    | 55.500  |
      | 435733    | 49.900  |
