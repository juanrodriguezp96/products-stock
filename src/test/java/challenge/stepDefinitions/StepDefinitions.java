package challenge.stepDefinitions;

import com.challenge.pages.PriceSmartProductPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class StepDefinitions {

    private PriceSmartProductPage priceSmartProductPage;

    @Given("I am in the PriceSmart product {string} page")
    public void iAmInThePriceSmartProductPage(String productId) {
        priceSmartProductPage = new PriceSmartProductPage(BaseTest.getDriver().getWebDriver(), productId);
    }

    @When("I check the product {string} is available")
    public void iCheckTheProductIsAvailable(String productId) {
        String productTitle = priceSmartProductPage.getProductTitle();
        Assert.assertTrue(String.format("Product not available: %s | Code: %s", productTitle, productId),
                priceSmartProductPage.isProductAvailable());
    }

    @When("I check the price of {string} is {string}")
    public void iCheckThePrice(String productId, String price) {
        String productTitle = priceSmartProductPage.getProductTitle();
        String productPrice = priceSmartProductPage.getProductPrice();
        Assert.assertTrue(String.format("Product price different. Product: %s | Code: %s | Expected Price: %s - Actual Price: %s", productTitle, productId, price, productPrice), productPrice.contains(price));
    }

}
