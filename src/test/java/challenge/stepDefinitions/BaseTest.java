package challenge.stepDefinitions;

import com.challenge.Driver;
import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.Before;

import static com.challenge.utils.Utils.writeDataLineByLine;
import static com.challenge.utils.Utils.getData;


public class BaseTest {

    private static Driver driver;

    @Before
    public void beforeSuite(){
        driver = new Driver("chrome");
    }

    @After
    public void afterSuite(){
        if(driver != null){
            driver.getWebDriver().quit();
        }
    }

    public static Driver getDriver() {
        return driver;
    }

}
